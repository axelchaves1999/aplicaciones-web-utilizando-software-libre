<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;


class PostController extends Controller
{

    public function index()
    {

        return view('posts.index', [
            'posts' =>  Post::latest()->filter(request(['search', 'category', 'author']))->paginate(5),
            'currentCategory' => Category::where('slug', request('category'))->first()
        ]);
    }

    public function show(Post $post)
    {

        return view('posts.show', [
            'post' => $post
        ]);
    }
    public function create(Post $post)
    {

        return view('posts.create');
    }

    public function admin(){

        return view('admin.posts.create');
    }



}
