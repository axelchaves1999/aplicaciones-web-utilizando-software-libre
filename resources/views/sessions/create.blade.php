@include('layout')

<section class="px-6 py-8">
    <main class="max-w-lg mx-auto">
        <h1 class="text-center font-bold text-xl">Log In</h1>

        <form method="POST" action="/login" class="mt-10">
            @csrf
            <div class="mb-6">


                <label class="block mb-2 uppercase text-xs " for="name">
                    email
                </label>
                <input class="border border-gray-400 p-2 w-full"
                    type="email"
                    name="email"
                    id="email"
                    required
                    value="{{old('email')}}"
                >

                @error('email')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                @enderror



                <label class="block mb-2 uppercase text-xs ">
                    Password
                </label>
                <input class="border border-gray-400 p-2 w-full"
                    type="password"
                    name="password"
                    id="password"
                    required
                >
                @error('password')
                    <p class="text-red-500 text-xs mt-1">{{$message}}</p>
                @enderror


                <div class="mt-6">
                    <button type="submit"
                    class="bg-blue-400 text-white rounded py-2 px-4 hover:bg-blue-500">
                    Log in
                    </button>
                </div>

            </div>

        </form>
    </main>
</section>

