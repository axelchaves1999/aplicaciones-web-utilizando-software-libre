<!doctype html>

<title>Laravel From Scratch Blog</title>
<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
<script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>

<body style="font-family: Open Sans, sans-serif">
    <section class="px-6 py-8">
        <nav class="md:flex md:justify-between md:items-center">
            <div>
                <a href="/">
                    <img src="/images/logo.svg" alt="Laracasts Logo" width="165" height="16">
                </a>
            </div>

            <div class="mt-8 md:mt-0 flex">
                @auth
                    <x-dropdown>
                        <x-slot name="trigger">
                            <button @click="show = !show"
                                class="py-2 pl-3 pr-9 text-sm font-semibold">Welcome,{{ auth()->user()->name }}!</button>
                        </x-slot>
                        @can('admin')
                            <x-dropdown-item href="/admin/posts">
                                Dashboard
                            </x-dropdown-item>
                            <x-dropdown-item href="/admin/posts/create" :active="request()->is('admin/posts/create')">
                                New Post
                            </x-dropdown-item>
                        @endcan
                    </x-dropdown>


                    <form method="POST" action="/logout" class=" text-xs font-semibold text-blue-500 ml-6 mb-6 mr-6">
                        @csrf
                        <button type="submit">Log out </button>
                    </form>
                @else
                    <a href="/register" class="text-xs font-bold uppercase">Register</a>
                    <a href="/login" class="ml-6 text-xs font-bold uppercase">Log in</a>
                @endauth

                <a href="#newsletter"
                    class="bg-blue-500 ml-3 rounded-full text-xs font-semibold text-white uppercase py-3 px-5">
                    Subscribe for Updates
                </a>
            </div>
        </nav>




        @if (session()->has('success'))
            <div class="fixed bg-blue-500 text-white py-2 px-4 rounded-xl bottom-3 right-3 text-sm">
                <p>
                    {{ session('success') }}
                </p>
            </div>
        @endif
</body>
