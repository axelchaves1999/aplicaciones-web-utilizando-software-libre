@props(['trigger'])

<div x-data="{ show: false}" >

    <div  @click.away="show = false">
        {{$trigger ?? ''}}
    </div>

    <div x-show="show" class="py-2 absolute" >
       {{ $slot}}
    </div>

</div>
