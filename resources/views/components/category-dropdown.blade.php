<x-dropdown>
    <x-slot name="trigger">
        <button @click="show = !show" class="py-2 pl-3 pr-9 text-sm font-semibold">{{ isset($currentCategory) ? $currentCategory->name : 'Categories'}}</button>
    </x-slot>
    <x-dropdown-item href="/">
        all
    </x-dropdown-item>
    @foreach ($categories as $category)
        <x-dropdown-item
        href="?category={{$category->slug}}"

        >{{ucwords($category->name)}}</x-dropdown-item>

    @endforeach
</x-dropdown>
