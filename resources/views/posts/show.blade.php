

@include('layout')

    <section class="px-6 py-8">
        <main class="max-w-6xl mx-auto mt-6 lg:mt-20 space-y-6">
            <article
                class="transition-colors duration-300 hover:bg-gray-100 border border-black border-opacity-0 hover:border-opacity-5 rounded-xl">
                <div class="py-6 px-5 lg:flex">
                    <div class="flex-1 lg:mr-8">
                        <img src="storage/{{$post->thumbnail}}" alt="Blog Post illustration" class="rounded-xl">
                    </div>

                    <div class="flex-1 flex flex-col justify-between">
                        <header class="mt-8 lg:mt-0">
                            <div class="space-x-2">
                                <a href="?category={{$post->category->slug}}"
                                   class="px-3 py-1 border border-blue-300 rounded-full text-blue-300 text-xs uppercase font-semibold"
                                   style="font-size: 10px">{{$post->category->name}}</a>

                                <a href="/"
                                   class="px-3 py-1 border border-red-300 rounded-full text-red-300 text-xs uppercase font-semibold"
                                   style="font-size: 10px">go back</a>
                            </div>

                            <div class="mt-4">
                                <h1 class="text-3xl">
                                    {{$post->title}}
                                </h1>

                                <span class="mt-2 block text-gray-400 text-xs">
                                        Published <time>{{$post->created_at->diffForHumans()}}</time>
                                    </span>
                            </div>
                        </header>

                        <div class="text-sm mt-2">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                                ullamco laboris nisi ut aliquip ex ea commodo consequat.
                            </p>

                            <p class="mt-4">
                                Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                            </p>
                        </div>

                        <footer class="flex justify-between items-center mt-8">
                            <div class="flex items-center text-sm">
                                <img src="./images/lary-avatar.svg" alt="Lary avatar">
                                <div class="ml-3">
                                    <h5 class="font-bold">
                                        <a href="/?author= {{$post->author->name}}">{{$post->author->name}}</a>
                                    </h5>
                                </div>
                            </div>


                        </footer>
                    </div>
                </div>
                <section class="col-span-8 col-start-5 mt-10 space-y-6">
                    @auth
                    <form method="POST" action="/posts/{{$post->slug}}/comments" class="border border-gray-200 p-6 rounded-xl">
                        @csrf
                        <header class="flex items-center">
                            <img src="https://i.pravatar.cc/60" alt="" width="60" height="60" class="rounded-xl">
                            <h2>want to participade?</h2>
                        </header>
                        <div class="mt-6">
                            <textarea name="body" class="w-full"  rows="5" placeholder="Quick,thing of something to say!"></textarea>
                            @error('body')
                                <span>{{$message}}}</span>

                            @enderror
                        </div>
                        <div class="flex justify-end">
                            <button type="submit" class="bg-blue-500 text-white uppercase font-semibold
                            text-xs py-2 px-10 rounded-2xl ">Post</button>
                        </div>

                    </form>

                    @endauth

                    @foreach ($post->comments  as $comment)
                        <x-post-comment :comment="$comment"/>
                    @endforeach
                </section>
            </article>


        </main>


    </section>

