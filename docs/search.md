[< Go to index](../README.md)

## Los fundamentos

### ¿Cómo se hace un search en laravel?

En el ejemplo que estamos viendo en el curso primero se hace una solicitud a la base de datos para traer todos los posts y despues hacemos una busqueda

Solicitud a la base de datos

```php
 $posts = Post::latest();
```

Filtrado de datos

```php
 $posts->where('title','like','%' .request('search') . '%')->orWhere('body','like','%' .request('search') . '%');
```

La forma más eficiente de realizar una busqueda seria crear un scope en el modelo

```php
  public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? false, function ($query, $search) {
            $query
                ->where('title', 'like', '%' . $search . '%')
                ->orWhere('body', 'like', '%' . $search . '%');
        });
    }
```

Crear un controlador, esto lo podemos hacer desde `php artisan make:controller PostController`

Y dentro del controlador crear un funcion para el metodo

```php
  public function index()
    {

        return view('posts', [
            'posts' =>  Post::latest()->filter(request(['search']))->get(),
            'categories' => Category::all()
        ]);
    }
```
