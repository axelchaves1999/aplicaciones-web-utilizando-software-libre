[< Go to index](../README.md)

## Comentarios

### Template

Primero debemos crear un componente comentario donde tendremos la estructura de nuestro comentario

```html
<article class="flex bg-gray-100 border border-gray-p-6 rounded-xl space-x-4">
    <div class="flex-shrink-0">
        <img src="https://i.pravatar.cc/60" alt="" width="60" height="60" class="rounded-xl">
    </div>

    <div>
        <header>
            <h3 class="font-bold">John Doe</h3>
            <p class="text-ts">
                Posted
                <time>8 months ago </time>
            </p>
        </header>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Error odit dolore maiores natus quis adipisci ullam accusantium! Ipsa modi nemo ex ducimus quod sapiente dolor nulla voluptates, ullam obcaecati eaque.</p>
    </div>

</article>
```

Esto lo debemos agregar en nuestra vista _show_ donde existiran los comentarios


### Comentarios dinamicos

Para crear nuestro comentarios debemos crear un modelo, una migracion y un factory, con el siguiente comando creamos todo lo anterior `php artisan make:model Comment -mfc`

En nuestra migracion agregamos
```php
   $table->id();
            $table->foreign('post_id')->constrained()->cascadeOnDelete();
            $table->unsignedBigInteger('user_id');
            $table->text('body');
            $table->timestamps();



```

### Comments dynamic

Lo primero que debemos hacer es en el factory de comments agregar lo siguiente

```php
 public function definition()
    {
        return [
            'post_id' => Post::factory(),
            'user_id'=> User::factory(),
            'body'=>$this->faker->paragraph()
        ];
    }
```
En nuestro modelo _Post_ agregar un asociacion con el modelo Comment

```php
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

```

Y en el modelo Comments agregatr una asociacion con el modelo _post_ y el _user_
```php
    public function post(){
        return $this->belongsTo(Post::class,'post_id');
    }

    public function author(){
        return $this->belongsTo(User::class,'user_id');
    }
```
Con lo anterior ya hecho lo unico que debemos hacer es mostrar los comentarios

```php
    @foreach ($post->comments  as $comment)
        <x-post-comment :comment="$comment"/>
    @endforeach
```

### Activacion de form para comentarios

Lo primero que debemos hacer es crear un controllador para nuestro comentario, el controlador
tendra las acciones para administrar un comentario

```php
public function store(Post $post){
    request()->validate([
        'body'=> 'required'
    ]);

    $post->comments()->create([
        'user_id'=>request()->user()->id,
        'body'=>request('body')
    ]);
    return back();
}

```

Con nuestro controlador credo debemos de crer un metodo al que llave el controlador, en este caso 
_store_ para que el comentario pueda ser almacenado

Despues creamos la ruta para nuestro comentario

```
Route::post('posts/{post:slug}/comments',[PostCommentsController::class,'store']);
```

Con la ruta ya creada y el controlador funcionando debemos tener en cuenta algunos detalles como
` protected $guarded = [];` para que el comentario pueda llevar todos los atributos y debemos verificar con 
_@auth_ que nuestro usuario este logueado para mostrar el form, de otra forma no lo haria

Si añadimos _@error en el cuerpo del formulario del comentario podremos capturar errores del mismo

```php
    @error('body')
        <span>{{$message}}</span>
    @enderror
```
