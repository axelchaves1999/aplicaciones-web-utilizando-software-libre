[< Go to index](../README.md)

## Admin section

### Lo primero que debemos crear una vista y una ruta en nuestros routes, `Route::get('admin/posts/create',[PostController::class,'create']);` Seria la ruta, en el controlador _PostController_ creamos un metodo llamado _create_

```php

    public function create(Post $post)
    {

        if(auth()->guest()){
            abort(403);
        }

        return view('posts.create');
    }
```

Sera el encargado de validar si se puede acceder a la vista

Con
`php artisan make:middleware MustBeAdministrador` creamos el middleware, añadimos la siguiente logica en el middleware

```php
        if(auth()->guest()){
            abort(403);
        }
        if(auth()->user()->name !== 'Axel'){
            abort(403);
        }
        return $next($request);
```

Y en nuesta ruta indicamos el middlaware que queremos usar

```php
[PostController::class,'create'])->middleware('admin');

```

Middleware ` 'admin' => MustBeAdministrador::class,`


### Para crear un post debemos de ajustar nuestr ruta

```php
Route::post('admin/posts',[PostController::class,'store'])->middleware('admin');
```

Crear un metodo store

```php
public function store(){
        try {
            $atributes = request()->validate([
                'title' => 'required',
                'slug' => ['required', Rule::unique('posts', 'slug')],
                'excerpt' => 'required',
                'body' => 'required',
                'category_id' => ['required', Rule::exists('categories', 'id')]
            ]);

            $atributes['user_id'] = Auth::user()->id;
            Post::create($atributes);

            return back();
        } catch (\Throwable $th) {
            ddd($th);
    }

    }
```

En mi caso ajsutar este atributo

```php
  protected $guarded = [];

```
### Eliminar un post

En el caso del curso lo que hicimos es crear un nuevo controlador llamado _adminPostController_, desde este controlador tendros acciones como editar y borrar 

Para eliminar un posts debemos crear una ruta `Route::delete('admin/posts/{post:id}',[AdminPostController::class,'destroy'])->middleware('admin');`

Creamos el metodo

```php

    public function destroy(Post $post)
    {
        $post->delete();

        return back()->with('success', 'Post Deleted!');
    }

```

### Editar un post

Igual que el caso anterior creamos una ruta en nuestro controlador _adminPostController_
`Route::patch('admin/posts/{post:id}',[AdminPostController::class,'update'])->middleware('admin');`

Creamos el metodo _update_

```   php
public function update(Post $post)
    {
        $attributes = $this->validatePost($post);

        if ($attributes['thumbnail'] ?? false) {
            $attributes['thumbnail'] = request()->file('thumbnail')->store('thumbnails');
        }

        $post->update($attributes);

        return back()->with('success', 'Post Updated!');
    }

```
