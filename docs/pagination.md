[< Go to index](../README.md)

## Paginacion

### ¿Cómo se hace una paginacion en laravel?

Podemos lograrlo desde nuestro controllador, en nuestro metodo index colocamos lo siguiente

```php
'posts' =>  Post::latest()->filter(request(['search','category']))->paginate(5),

```

Y en nuestra vista 

```php
 {{$posts->links()}}
```

Esto nos permitira cargar la paginacion automaticamente, esto es posible gracias a tailwind

Tambien podemos agregar paginacion simple que la principal diferencia es que no calcula la cantidad de items por pagina
