[< Go to index](../README.md)

## Filtering

### where exists

Algo interesante es que en sql se puede utilizar el _exists_ que recibe como parametro un sql,
si el sql devuelve un valor verdadero entonces se ejecutara el query pasado por parametro

Ejemplo

```sql
select * from posts where exist(select * from categories)
```

Ejemplo de busqueda por categoria

```php
       $query->when($filters['category'] ?? false, function ($query, $category) {
            $query
                ->whereExists(fn($query) =>
                    $query->from('categories')
                    ->whereColumn('categories.id','posts.category_id')
                    ->where('categories.slug',$category)
        );
```

Con la nueva busqueda usamos una expresion diferente para la busqueda de la categoria

```php
        $query->when($filters['category'] ?? false, function ($query, $category) {
            $query->whereHas('category',fn($query) =>
            $query->where('slug',$category));
        });

```

Agregando en nuestro blade _post-header_

```php
    @foreach ($categories as $category)
        <x-dropdown-item
            href="/?categories/{{$category->slug}}"

        >{{ucwords($category->name)}}</x-dropdown-item>

     @endforeach
```

En mi post controller

```php
  return view('posts', [
            'posts' =>  Post::latest()->filter(request(['search','category']))->get(),
            'categories' => Category::all(),
            'currentCategory'=> Category::where('slug',request('category'))->first()
        ]);

```

Con el comando `php artisan make:component CategoryDropdown` podemos crear un componente

```php
  public function render()
    {
        return view('components.category-dropdown',[
            'categories' => Category::all(),
            'currentCategory'=> Category::where('slug',request('category'))->first()
        ]);
    }
```

Esto nos permite admnistrar ese componente de una forma mas directa

### Merge Category and Search Query

```php
    <form method="GET" action="/">
        @if(request('category'))
            <input type="hidden" name="category" value="{{request('category')}}">
        @endif
        <input type="text" name="search" placeholder="Find something" class="bg-transparent placeholder-black font-semibold text-sm" value="{{request('search')}}">
    </form>
```


```php
    @foreach ($categories as $category)
        <x-dropdown-item
        href="/?categories/{{$category->slug}}&{{http_build_query(request()->except('category'))}}"

        >{{ucwords($category->name)}}</x-dropdown-item>

    @endforeach

```

### Fix a Confusing Eloquent Query Bug


```php
    $query->when($filters['search'] ?? false, fn($query, $search) =>
        $query->where(fn($query)=>

            $query->where('title', 'like', '%' . $search . '%')
            ->orWhere('body', 'like', '%' . $search . '%')

        )

    );

```
