[< Go to index](../README.md)

## APis

### Integracion Api

Lo primero que debemos hacer es registrarnos en _mailchimp.com_ solicitar una key,
guardarla en _env_

```php
MAILCHIP_KEY = 327665xxxc0xfac0xx78efe014xxxx-xx
```

Conectarnos al api por medio de una ruta

```php
Route::get('ping',function(){
    $mailchimp = new \MailchimpMarketing\ApiClient();

    $mailchimp->setConfig([
	'apiKey' => config('services.mailchip.key'),
	'server' => 'us5'
    ]);

    $response = $mailchimp->ping->get();
    ddd($response);
});
```

Luego para probar la APi añadimos un miembro a la lista para comprobar que funcione

```php
    $mailchimp = new \MailchimpMarketing\ApiClient();

    $mailchimp->setConfig([
	'apiKey' => config('services.mailchip.key'),
	'server' => 'us5'
    ]);

    $response = $mailchimp->lists->addListMember('9cc37afbfa',[
        'email_address' => 'mynewscoverv2@gmail.com',
        'status' => 'subscribed'
    ]);

    ddd($response);

```

### Extract the newsletter service

Lo primero que haremos es cambiar nuestra ruta

```php
Route::post('newsletter',function(Newsletter $newsletter){
    request()->validate(['email'=> 'required|email']);

    try {
        $newsletter->subscribe(request('email'));
    } catch (\Exception $e) {
        return redirect('/')->with('Error','Invalid email');
    }


    return redirect('/')->with('success','You are now signed up for our newsletter');
});

```

Despues crearemos un directorio services en _\app_ añadiremos una clase _newsletter_ la cual contendra el metodo _subscribe_

```php
 public function subscribe(string $email,string $list = null)
    {
        $list ??= config('services.mailchimp.lists.susbscribers');

        $mailchimp = new ApiClient();

        $mailchimp->setConfig([
            'apiKey' => config('services.mailchimp.key'),
            'server' => 'us5'
        ]);

        return $mailchimp->lists->addListMember(config('services.mailchimp.lists.subscribers'), [
            'email_address' => $email,
            'status' => 'subscribed'
        ]);
    }
```

En el archivo env tendremos nuestra configuracion y en services extraeremos esa configuracion



### Containers

Lo primero que debemos hacer es dirigirnos a _AppServiceProvider.php_ donde en
` public function register(){}` añadiremos el siguiente codigo

```php
    app()->bind(MailchimpNewsletter::class,function(){
            $client = (new ApiClient)->setConfig([
                'apiKey' => config('services.mailchimp.key'),
                'server' => 'us5'
            ]);
            return new MailchimpNewsletter($client);
        });
```

Cambiaremos el nombre del archivo _newsletter_ por _ChimpNewsletter_ y crearemos el archivo _ConvertKitNewsletter_

Crearemos una interfaz _newsletter_ y la implementaremos en _ChimpNewsletter_ y en _ConvertKitNewsLetter_

