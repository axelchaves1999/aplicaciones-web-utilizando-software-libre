[< Go to index](../README.md)


## Integrate-the-design

### Trabajando con una plantilla

En el curso se nos presenta una plantilla en la que vamos a trabajar, aplicamos
los conocimientos aprendidos en los modulos de blade ademas se nos presenta funciones interesantes como `diffForHumans()` que transforma fechas.

Otro metodo interesante es el `skip(1)` que no reccore el elemento de una lista dado por parametro.

Declarando `@props(['post'])` soy capaz de pasar mis posts a otro blade para mostrarlos
`<x-posts-grid :posts="$posts"/>`

### Dropdown con javaScript

Lo primero que haremos es pegar el cdn de alphine en nuestro _posts-header.blade.php_ ` <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>`

Se puede asignar un nombre a las rutas de esta manera

```php
Route::get('/', function () { 
    $posts = Post::latest()->with('category')->get();
    return view('posts', [
        'posts' => $posts,
        'categories' => Category::all()
    ]);
})->name('home');

``
