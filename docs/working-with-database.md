[< Go to index](../README.md)


## Working with database


### Configuracion varaibles de entorno

En nuestro proyecto podemos encontrar las variables de entorno en el archivo .env
aqui nos enfocamos en cambiar las variables de base de datos

```sql
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=blog
DB_USERNAME=laravel
DB_PASSWORD=secret
```

### Configuraciones basicas

Lo primero que tenemos que hacer es asegurarnos que el entorno donde estamos trabajando este configurado con mysql lo ideal seria crear un usuario, una base de datos y darle privilegios a ese usuario sobre la base de datos, en entornos reales se crean varios usuarios para diferenes acciones en la base de datos;

En nuestro ambiente de desarrollo nos conectamos con:
`sudo mysql - u root -p` 

Creamos usuario con:
`CREATE USER laravel INDENTIFIED BY secret`

Creamos base de datos:
`CREATE DATABASE blog`

Asignamos privilegios:
`GRANT ALL PRIVILEGES ON blog TO laravel`

Hacemos un flush:
`FLUSH PRIVILEGES`

Despues de crear nuestra base de datos ejecutamos una migracion
`php artisan migrate`


### Eloquent 

Con Eloquent podemos ejecutar codigo, seleccionar colecciones y manipular colecciones

Podemos ingresar de esta forma:
`php artisan tinker`

Creamos un usuario:
`$user = new App\Models\User;`

Añadimos atributos:
`$user->name = 'Axel'`

Guardamos el usuario:
`$user->save()`

### Creando un modelo

Podemos crear un modelo con tan solo ejecutando:

Esto creara una migracion
```php
    php artisan make:migration create_posts_table
```

Editando esa migracion
```php
public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('excerpt');
            $table->text('body');
            $table->timestamps();
            $table->timestamp('published_at')->nullable();
        });
    }
```

Creando del modelo
```php
php artisan make:model Post
```

### Actualizando un registro

Desde Eloquent podemos manipular los objetos que se encuentran en nuestra base de datos
esto nos permite editar los registros, con anteriomente vimos es la misma secuencia de crear un registro lo unico que cambia es que hay que seleccionar ese registro y editar sus atributos


### Formas create registros

Asignamos permisos
```php
protected $fillable = ['title','excerpt','body'];
```

Ahora podemos crear un objeto de esta forma
```php
Post::create(['titile=>'This is the title','excerpt=>'This is the excerpt of post','body'=> 'This is the body']);

``

Asi podemos que algun atributo sea asignado de manera masiva
```php
protected $guarded = [id];
```

### Biding en nuestro archivo route

Esto es posible porque tenemos en nuestra ruta tenemos un key Eloquited lo hace

```php
Route::get('/posts/{post}', function (Post $post) {
    return view('post', [
        'post' =>  $post
    ]);
});
```

Podemos buscar un registro no solo por su id si no por sus atributos de esta forma
```php
Route::get('/posts/{post:slug}', function (Post $post) {
    return view('post', [
        'post' =>  $post
    ]);
});
```

### Relaciones con Eloquent

La forma en que se relacionan dos modelos con eloquent es el mismo concepto existenten entre relaciones entre base de datos relacionales 

En nuestra migracion indicamos un campo para colocar el id de la categoria

Algo que cambia el laravel es la funcion que me hace la relacion
```php
    public function category(){
        return $this->belongsTo(Category::class);
    }
```

Otra forma que podemos hacer es ver que modelo tiene realacion con otro modelo 

```php
 public function posts(){
        return $this->hasMany(Post::class);
    }

```
### Leasy loading con laravel

Podemos lograr una forma eficiente de cargar registros con el siguiente codig

```php
Route::get('/', function () { 
    $posts = Post::with('category')->get();
    return view('posts', [
        'posts' => $posts
    ]);
});
```

### Database seeding 

Esta funcion de php nos permite que cada vez que ejecutamos un migrate:fresh tengo una data que no se nos borre

```php
   public function run()
    {
        User::truncate();
        Category::truncate();

        $user = User::factory()->create();
        Category::create([
            'name'=> 'Sports',
            'slug'=> 'Sports' 
        ]);

        $family = Category::create([
            'name'=> 'Personal',
            'slug'=> 'Personal' 
        ]);

        Post::create([
            'user_id'=> $user->id,
            'category_id'=> $family->id,
            'title'=> 'My family post', 
            'slug'=> 'my-first-post',
            'excerpt'=> 'Lorem ipsum',
            'body'=> 'Lorem ipmsu dolor sit amet, consecteur adispicin elit. Morbi vivera'
            
        ]);
    }
```
Y con el comando `php artisan db:seed` podemos cargar esa data a nuestra base de datos


### Factory

Esta opcion me permite una forma rapida de poder crear algun objeto
en la base de datos

Algo interesante es que cada eloquent model creado tendra acceso a las funciones de `Factory`

```php
   use HasFactory;
```

Para crear un una Factory debemos ejecutar
```php
php artisan make:factory PostFactory
```

Con el comando `php artisan make:model Comment -mf` podemos crear

- Modelo
- Factory
- Migracion
