[< Go to index](../README.md)

## Los fundamentos

### ¿Cómo se carga una vista en laravel?

El archivo de rutas está ubicado en `/routes/web.php`.

Las rutas pueden llamar una vistar generada por un html o de un _blade_.


```php
Route::get('/', function () {
    return view('welcome');
});
```
Se puede devolver texto

```php
Route::get('/hello', function () {
    return 'hello world';
});
```
Se puede devolver un JSON

```php
Route::get('/hello', function () {
    return ['foo'=> 'bar'];
});
```

### ¿Cómo se vincula archivos css o .js ?

Se puede indicar un ruta relativa con `/` esto indicara que nuestros archivos
estan en `/public`.


### ¿Cómo se crea una vista?

Se  puede crear una nueva entrada en la carpeta `/routes` y depues
su respectiva vista en la carpeta `/views`.


### ¿Cómo se crea redireciones automaticas y contenido?

Se puede crear direcciones automaticas desde el archivo donde tenemos nuestras rutas en mi caso `routes/web.php`, una ruta puede contener el protocolo, el subdomino, el dominio, la terminación, la carpeta a la cual ingresaremos en este caso `posts` y la pagina que queremos ver en 'nuestro caso la queremos dimanica'

Las rutas en laravel contien la ruta visitada, la funcion que se ejecuta y los parametros, en nuestro caso lo que hacemos es recibir el parametro y lo concatenamos a una ruta para que de esta forma puede ser dimanica 

```php
$path = (__DIR__ . '/../resources/posts/'.$slug.'.html');
```

Para el contenido dinamico solo necesitamos sacar el contenido de la anterior ruta y 
añadirlo pasarlo con un argumento a nuestro blade

```php
return view('post',[
        'post' => $post
    ])
```

En nuestro blade

```php
<article>
       <?= $post; ?>
</article>
```

### ¿Cómo añadimos filtros en nuestra url?

Podemos indicar que tipo de parametros podemos recibir en nuestra url como la expresión
`where`, de esta manera podemos inidicar que caracteres podemos recibir en nuestra url

```php
Route::get('/posts/{post}', function ($slug) {
    $path = (__DIR__ . '/../resources/posts/'.$slug.'.html');
    if(!file_exists($path)){
        return redirect('/');
    }

    $post = file_get_contents($path);
    return view('post',[
        'post' => $post
    ]);
})->where('post','[A-z_\-]+');

```

### ¿Cómo cacheamos direcciones?

Laravel nos facilita la vida gracias a sus funciones que incluye 

```php
cache()->remember("posts.{$slug}",5,function() use($path){
        var_dump('file_get_contents');
        return file_get_contents($path);
    });
```

Este methodo recibe la ruta que queremos cachear, cuanto tiempo la queremos cachear un parametro extra para usar variables locales


### Usando filesystem Class

La clase filessystem nos da una gran variedad de metodos para poder manejar con 
facilidad nuestros directorios, como estamos tratando de crear un blog esto nos permitira
manejar las entradas del blog de una forma muy sencilla 

Lo primero que debemos hacer es crear un clase `Post`, con el siguiente contenido

```php
   public static function find($slug)
    {
        base_path();
        if (!file_exists($path = resource_path('posts/' . $slug . '.html'))) {
            throw new ModelNotFoundException();
        }
        
        return cache()->remember("post.{$slug}",1200,fn()=> file_get_contents($path));
    }
```
Como podemos observar el clase se encarga de buscar si algun directorio existe, si existe
es capas de cachear ese directorio y devolver el contenido de ese directorio en especifico

Si queremos hacer lo mismo pero con varios directorios solo debemos hacer lo siguiente

```php
public static function all(){
    $files =  File::files(resource_path('posts/'));
    return array_map(fn($file) => $file->getContents(),$files);
}
```
### Nuevos paquetes

Con el paquete YAML front matter podemos crear meta data en un archivo html
de esta forma podemos obtener esa metadata para nuestro benecifio para instalar el paquete solo necesitamos ejecutar `composer require spatie/yaml-front-matter`

Y para su utilizacion solo necesitamos importarlo.

Para el ejemplo del video con la clase ya creada `Post` lo que realizamos fue añadir 
algunos atributos y un constructor de la clase

```php
 public $tile;
    public $excerpt;
    public $date;
    public $body;
    public $slug;

    public function __construct($tile,$excerpt,$date,$body,$slug)
    {
        $this->title = $tile;
        $this->excerpt = $excerpt;
        $this->date = $date;
        $this->body = $body;
        $this->slug = $slug;
    }
```

Lo siguiente que hicimos fue hacer uso de nuestro nuevo paquete que nos permite acceder a rutas y extraer la meta data de un archivo 

```php
$files =  File::files(resource_path('posts/'));
    $posts = [];

    return collect($files)
        ->map(function($file){
            $document = YamlFrontMatter::parseFile($file);

            return  new Post(
                $document->title,
                $document->excerpt,
                $document->date,
                $document->body(),
                $document->slug
            );
        });

```

De esta forma somos capaces de manipular los datos mejor en los archivos _blade_.

```php
<article>
        <h1><?= $post->title ?></h1>

        <div>
            <?= $post->body; ?>
        </div>
    </article>

```

### Ordernar por asec o desc 

Pomos indicar a una lista o colección como queremos que liste los datos añadiendo `->sortByDesc('date');`

En nuestro caso esto tiene como efecto que muestro los posts mas recientes de primero 


Tambien con `cache()->rememberForever('posts.all', function (){})` somo capaces de cachear por un tiempo indifinido nuestros posts

