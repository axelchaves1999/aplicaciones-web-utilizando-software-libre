[< Go to index](../README.md)


## Blade

### ¿Qué es blade?

Los archivos con la extension blade me sirven como un template para mis vistas
lo que me permite utilizar algo parecido a ecmascript en javaScript, algo que facilita muchisimo la interesaccion con los archivos

Ejemplo:

```php
    @foreach ($posts as $post)
        <article>
            <h1>
                <a href="/posts/{{$post->slug}}"> {{ $post->title}}</a>
               
        
            </h1>

            <div>
                {!! $post->body !!}
            </div>
        </article>

    @endforeach
```

### Layout files

La forma mas simple de crear un layout seria crear un archivo en mi caso lo llame _layout_. En este archivo coloque la meta data perteneciente a un archivo html 5

```html
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    @yield('content')

</body>

</html>
```

Cree la etiqueta _@yield('content')_  y en mi archivo posts.blade unicamente deje mi logica para esa vista

```php
@extends ('layout')

@section('content')

@foreach ($posts as $post)
<article>
    <h1>
        <a href="/posts/{{$post->slug}}"> {{ $post->title}}</a>


    </h1>

    <div>
        {!! $post->body !!}
    </div>
</article>

@endforeach

@endsection

```

Otra forma de crear layouts seria la siguiente

```php
<x-layout>
    @foreach ($posts as $post)
    <article>
        <h1>
            <a href="/posts/{{$post->slug}}"> {{ $post->title}}</a>


        </h1>

        <div>
            {!! $post->body !!}
        </div>
    </article>
    @endforeach

</x-layout>

```