[< Go to index](../README.md)

## Forms y autentificaiocn

### Crear ruta para el form

Se puede crar un controlador `php artisan make:controller RegisterController`

En nuestro controlador _RegisterController_ creamos un metodo llamado `create`
que nos hara la funcion de enviarnos al formulario

```php
    public function create(){
        return view('register.create');
    }
```

Y crearemos un metodo store para registrar nuestro usuarios

```php
   public function store(){
        $attributes = request()->validate([
            'name'=> 'required',
            'email' => 'required|email',
            'password' => 'required'
        ]);
        User::create($attributes);
        return redirect('/');
    }

```

### Automatic password hashing

Una manera de encriptar nuestra constraseña seria esta forma 

```php
$attributes['password'] = bcrypt($attributes['password']);
```

En nuestro modelo de usuario, los modelos al declarar un funcion _set_ + un _el nombre del atributo_ + _Attribute_
cada vez que se cree un objecto esta funcion se ejecutara, entonces creamos lo siguiente

```php
    public function setPasswordAttribute($password){
        $this->attributes['password'] = bcrypt($password);
    }

```


### Failed Validation

Laravel nos brinda una propiedad para hacer validaciones

```php
    @error('name')
        <p class="text-red-500 text-xs mt-1">{{$message}}</p>
    @enderror

```

Y una propiedad para recordar la informacion que existia en los inputs

```php
    value="{{old('name')}}"
```

Una forma de validar unique seria

```php
  'email' => 'required|email|unique:users,email',
```

Otra forma

```php
['required|email|',Rule::unique('users','email')]

```

### Flash message

En nuestro registerController despues de crear nuestro usuario declaramos lo siguiente

```php
 session()->flash('success','Account has been created');
```

Para mostrar el mensaje solo necesitamos lo siguiente

```php
 @if(session()->has('success'))
        <div class="fixed bg-blue-500 text-white py-2 px-4 rounded-xl bottom-3 right-3 text-sm">
            <p>
                {{session('success')}}
            </p>
        </div>
    @endif
```

### Sing in

Esta propiedad nos hace el login `Auth::login($user)`


Para hacer log out creamos un controllador `SessionsController` en el cual creamos el metodo destroy

```php
  public function destroy(){
        Auth::logout();
        return redirect('/')->with('success','Goodbye');
    }

```

### Log in

Lo primero es crear nuestra ruta para el log in

```php
Route::post('login',[SessionsController::class,'store'])->middleware('guest');
```

Tambien debemos crear nuestro form para hacer inicio de session, una vez creados la ruta y el form lo que haremos es 
crear un metodo _store_ en nuestro controlador sessions, la funcion de este controlador es validar si la contraseña y el email son validos y hacer un login

```php
public function store(){
    $attributes = request()->validate([
            'email' => 'required|email',
            'password'=> 'required'
    ]);

    if(Auth::attempt($attributes)){
            return redirect('/')->with('success','Welcome Back');
    }

    return back()
        ->withInput()
        ->withErrors(['email'=> 'Wrong credentials']);

}

```


### Laravel breeze

Laravel nos ofrece un modulo completo para el registro, login, autentificacion, lo primero que se hizo fue crear un neuvo proyecto, despues instalar laravel breeze `composer require laravel/breeze --dev`

Cuando la instalacion ya este terminada lo siguiente que haremos es ejecutar el comando `php artisan breeze:install`, hay que tomar en cuenta que se debe terner node js instalado en la maquina anfitriona o en la que se esta ejecutando el proyecto, con esto lo unico que queda es ejecutar `npm install npm run dev`

