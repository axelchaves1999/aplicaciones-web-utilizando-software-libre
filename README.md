# Laravel from the Scratch

## Indice

-[Los fundamentos](./docs/the-basics.md)

-[Blade](./docs/blade.md)

-[Working with database](./docs/working-with-database.md)

-[Integrate the design](./docs/integrate-the-design.md)

-[Search](./docs/search.md)

-[Filtering](./docs/filtering.md)

-[Pagination](./docs/pagination.md)

-[Forms-and-authentication](./docs/forms-and-authentication.md)

-[Comments](./docs/comments.md)

-[Newsletter and APIs](./docs/Newsletter-and-APis.md)

-[Admin section](./docs/admin-section.md)
